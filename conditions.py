a,b=10,20;
if(a>b):
    print('a({}) is greater than b({})'.format(a,b))
else:
    print('b({}) is greater than a({})'.format(a,b))

print('a is greater' if a>b else 'b is greater')
