#Two kinds of Conditional expressions
x,y=12,14

if(x>y):
    print('x({} is greater than y({})'.format(x,y))
elif(y>x):
    print('y({}) is greater than x({})'.format(y,x))
else:
    print('x({}) is equal to y({})'.format(x,y))
                
a,b=2,4
#This below code snippet is called the conditional expression, just like how we use it via Ternary Operator
s='a({}) is greater than b({})'.format(a,b) if a>b else 'b({}) is greater than a({})'.format(b,a)
print(s)
