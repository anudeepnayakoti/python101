def main():
    #Defining a tuple
    tuple1=(1,2,3,4,5)
    tuple2=('Anudeep', 'Ankith', 'Sai manish')
    lister=['Chutky', 'Chumky', 'Sri Manish']
    string1='Anudeep'
    
    #looping tuple2
    for i in tuple2:
        print(i)
    print("----------------------")
    #Changing the index_values in listers
    lister.insert(0,'Anuradha')
    lister.insert(1,'Bikshapathi')
    lister.pop(2)# Pop() is used to remove item from the list using index_value
    lister.remove('Bikshapathi')#Used to remove the value specified in the parenthesis.
    #looping lister
    for z in lister:
        print(z)
if __name__ =="__main__":main()