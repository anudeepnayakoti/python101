#There are two numeric types:'Integer' and 'Float' @Anudeep Nayakoti
def main():
    a=int(12.37)
    print(type(a),a)
    print(float(a))
    b=12.33
    print(b, type(b))
    result=47/3
    print(type(result), result)#This gives Floating value
    print(47//3)#This is giving an Integer value instead of Floating value
    print(round(5/3, 4))#Rounds the result
    
if __name__=="__main__":main()#This is like the Static void main String args() in Java