def main():
    #One way of defining dictionary:
    dic1={'Adarsh':1201,
          'Anudeep':1202,
          'Anwesh':1203,
          'Chaitanya':1204,
          'Guru':1205}
    #dummy=sorted(dic1.items())
    #print(dummy)
    #Another way of defining dictionary:
    dic2=dict(
        dad=970, mum=984, bro=717
        )
    #Inserting an item into a dictionary:
    dic2['cop']=100
    #Looping the first dictionary
    for k in dic1:
        print(k, '-->',dic1[k])
    print(dic2)#Printing all the values plus the newly appended value
if __name__ == "__main__":main()
    