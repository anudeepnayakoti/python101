def main():
    x=42
    y=42
    print(x)
    print(id(x))
    print(id(y))
    print(x is y)
    x=dict(x=50)
    y={'x':50}
    print(type(y))
    print(type(x))
    print(x, id(x))
    print(y, id(y))
    print(x==y)#Compares the values as both of them are true
    print(x is y)#Compares the Id's of both objects.
if __name__=="__main__":main()
    