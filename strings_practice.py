x='This is string'
y=''' This is also a string '''
z=""" This seems to be a string as well """
print(type(x))
def main():
    #Poem way of writing text
    text='''\
    My name is Anudeep!
    My age is just a number
    '''
    #New-line using \n
    text2="Hello!\n This is new way of writing code"
    print(text)
    print(text2)
    text3=r" This is \n kind of text"
    print(text3)
if __name__ =="__main__":main()

        


