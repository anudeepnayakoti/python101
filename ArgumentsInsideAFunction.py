#Creating functions and pass arguments inside it.

def main():
    print('It is inside the main function')
    #We will be calling the func() from this block of code.

    func(2)
        
#Lets define another function that prints range of numbers
def func(a):
    for i in range(a,5):
        print(i)
            
if __name__=="__main__":main()
